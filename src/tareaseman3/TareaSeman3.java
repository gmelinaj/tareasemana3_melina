/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tareaseman3;

import java.util.Scanner;

/**
 *
 * @author Melina Gomez
 */
public class TareaSeman3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int opcion;
        Scanner sc = new Scanner(System.in);
        boolean sigue = true;
        while (sigue == true) {
            System.out.println("");
            System.out.println("-------Menú-------");
            System.out.println("1.Matriz");
            System.out.println("2.Nombre y Edad");
            System.out.println("3.Salir");
            System.out.print("Opcion: ");
            opcion = sc.nextInt();
            System.out.println("");
            switch (opcion) {
                case 1:
                    TareaMatriz mm = new TareaMatriz();
                    mm.matrt();
                    break;
                case 2:
                    DiccionarioNombreEdad dne = new DiccionarioNombreEdad();
                    DiccionarioNombreEdad.nombreedad();
                    break;
                case 3:
                    sigue=false;
                    break;

                default:
                    System.out.printf("Opcion no valida");
                    break;
            }

        }

    }

}
